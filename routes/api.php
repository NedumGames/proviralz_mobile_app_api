<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!v
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');
Route::get('sendpin/{name}/{email}/{phone}/{pin}', 'ApiController@sendpin');
Route::post('reset_password/{email}', 'ApiController@resetPassword');
Route::post('passwordchange/{id}/{token}', 'ApiController@password_change');
Route::post('googleregister', 'ApiController@googleRegister');
Route::post('googlelogin', 'ApiController@googleLogin');
Route::get('forgetpassword/{email}/{pin}', 'ApiController@ForgetPassword');
Route::post('resetpassword', 'ApiController@ResetPassword');

//get the music details
Route::get('musicplay/{id}', 'PostController@OpenMusic');



Route::group(['middleware' => 'auth:api'], function () {
    Route::get('logout', 'ApiController@logout');
    
    //for user operation
    Route::post('createuser', 'UserController@CreateUser');
    Route::post('updateuser', 'UserController@updateUser');
    Route::get('makeadmin/{id}/{type}', 'UserController@MakeAdmin');
    Route::get('getalluser', 'UserController@getAllUser');
    Route::get('banuser/{id}', 'UserController@BanUser');
    Route::get('getuserbyid/{id}', 'UserController@GetUserDetailsById');
    Route::get('getuserbyphone/{phone}', 'UserController@GetUserDetailsByPhone');
    Route::get('getuserbyemail/{email}', 'UserController@GetUserDetailsByEmail');
    Route::get('deleteuser/{id}', 'UserController@DeleteUserById');

    //for post management
    Route::post('createpost', 'PostController@CreatePost');
    Route::get('getallpost', 'PostController@getAllPost');
    Route::get('getallpostadmin', 'PostController@getAllPostAdmin');
    Route::get('getpostbyid', 'PostController@getPostById');
    Route::get('getpostbyuser/{user_id}', 'PostController@getPostByUser');
    Route::put('updatepost/{id}', 'PostController@updatePost');
    Route::put('updatepoststatus/{id}', 'PostController@updatePostStatus');
    Route::put('returnpoststatus/{id}', 'PostController@returnPostStatus');
    Route::delete('deletepost/{id}', 'PostController@DeletePost');

    //for comment management
    Route::post('createcomments', 'CommentsController@CreateComments');
    Route::delete('deletecomment/{id}', 'CommentsController@DeleteComment');
    Route::get('getallcomments/{post_id}', 'CommentsController@getAllComments');

    //not test start
    //for likes management
    Route::post('createlike', 'LikesController@CreateLike');
    Route::post('createdownloads', 'LikesController@CreateDownloads');
    Route::delete('deletedownloads/{userid}/{id}', 'LikesController@DeleteDownload');
    Route::delete('deletelike/{id}', 'LikesController@DeleteLikes');
    Route::get('viewdownloads/{owner}', 'LikesController@viewDownloads');
    

    //for views management
    Route::get('createviews/{post_id}', 'ViewsController@CreateViews');
    Route::post('viewearning', 'ViewsController@ViewsEarning');
    //the user in this case is the owner of the post viewed
    Route::get('getviewsbypost/{post_id}', 'ViewsController@GetViewsByPost');

    //to manage activity
    Route::get('getactivity/{owner_id}', 'ActivityController@getActivity');


    //for withdrawals management
    Route::post('makewithdrawal', 'TransactionController@makeWithdrawal');
    Route::get('approvewithdrawals/{id}', 'TransactionController@ApproveWithDrawals');
    Route::get('viewwithdrawals', 'TransactionController@viewWithdrawals');
    Route::get('viewwithdrawalsbyid/{id}', 'TransactionController@viewWithdrawalsById');
    Route::get('viewmytrans', 'TransactionController@viewMyTransaction');
//not tested end

    //for transactions

    //for earnings
});
