<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Posts;
use Auth;

class PostController extends Controller
{
    // for the post we need a cover, file path to the file they posted, 
    // description, comments, likes, views

    public function CreatePost(Request $request){
        if(Auth::user()){

            $post = new Posts();
            $post->user_id = Auth::user()->id;
            $post->cover = $request->cover;
            $post->description = $request->description;
            $post->artist = $request->artist;
            $post->audio = $request->audio;
            $post->video = $request->video;
            $post->title = $request->title;
            $post->genre = $request->genre;
            $post->status = 1;
            $post->save();

            return response()->json([
                'success' => true,
                'message' => "post published successfully.",
                'user' => $post
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
                'user' => $user
            ], 401); 
        }
    }

    public function getAllPost(){
        if(Auth::user()){
            $post = Posts::where('status', 1)->orderBy('id', 'desc')->get();

            return response()->json([
                'success' => true,
                'data' => $post,
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
                'user' => $user
            ], 401); 
        }
    }
    
    public function getAllPostAdmin(){
        if(Auth::user()){
            $post = Posts::orderBy('id', 'desc')->get();

            return response()->json([
                'success' => true,
                'data' => $post,
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
                'user' => $user
            ], 401); 
        }
    }
    
    public function updatePostStatus($id, Request $request){
                 
        if(Auth::user()){
            $token = "Bearer ".Auth::user()->api_token;
            if($token != $request->header('Authorization')){
                return response()->json([
                    'success' => true,
                    'message' => "Unauthorized user - you dont have the permission to update this post",
                ], 401);
            }

            $post = Posts::where('id', $id )->update([
                'status' => 1
                ]);

            return response()->json([
                'success' => true,
                'data' => $post,
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
                'user' => $user
            ], 401); 
        }
    }
    
    public function returnPostStatus($id, Request $request){
                 
        if(Auth::user()){
            $token = "Bearer ".Auth::user()->api_token;
            if($token != $request->header('Authorization')){
                return response()->json([
                    'success' => true,
                    'message' => "Unauthorized user - you dont have the permission to update this post",
                ], 401);
            }

            $post = Posts::where('id', $id )->update([
                'status' => 0
                ]);

            return response()->json([
                'success' => true,
                'data' => $post,
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
                'user' => $user
            ], 401); 
        }
    }

    public function getPostById(){
        if(Auth::user()){
            $post = Auth::user()->post;//::find(Auth::user()->id);

            return response()->json([
                'success' => true,
                'data' => $post,
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
                'user' => $user
            ], 401); 
        }
    }
    
    public function OpenMusic($id){
        $post = Posts::find($id);

        return response()->json([
                'success' => true,
                'data' => $post,
            ], 200);
    }

    public function getPostByUser($user_id){
        if(Auth::user()){
            $user = User::find($user_id);
            $post = $user->post;
            return response()->json([
                'success' => true,
                'data' => $post,
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
                'user' => $user
            ], 401); 
        }
    }

    public function updatePost($id, Request $request){
        if(Auth::user()){
            $token = "Bearer ".Auth::user()->api_token;
            if($token != $request->header('Authorization')){
                return response()->json([
                    'success' => true,
                    'message' => "Unauthorized user - you dont have the permission to update this post",
                ], 401);
            }

            $post = Posts::find($id);
            $post->user_id = Auth::user()->id;
            $post->cover = $request->cover;
            $post->description = $request->description;
            $post->artist = $request->artist;
            $post->audio = $request->audio;
            $post->video = $request->video;
            $post->title = $request->title;
            $post->genre = $request->genre;
            $post->save();

            return response()->json([
                'success' => true,
                'data' => $post,
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
                'user' => $user
            ], 401); 
        }
    }

    public function DeletePost($id, Request $request){
        if(Auth::user()){
            $token = "Bearer ".Auth::user()->api_token;
            if($token != $request->header('Authorization')){
                return response()->json([
                    'success' => true,
                    'message' => "Unauthorized user - you dont have the permission to update this post",
                ], 401);
            }

            $post = Posts::find($id);
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => "post deleted successfully.",
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
                'user' => $user
            ], 401); 
        }
    }
}
