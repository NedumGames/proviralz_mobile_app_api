<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterAuthRequest;
use App\User;
use App\Posts;
use Auth;

class UserController extends Controller
{
    //create users - Admin
    public function CreateUser(Request $request){

        $auth_user = Auth::user()->type;

        if($auth_user == 0) {
            $user = new User();
            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->type = $request->type;
            $user->password = bcrypt($request->password);
            $user->save();  
            
            if(!$user) 
                return response()->json([
                    'success' => true,
                    'message' => "Unable to create user",
                    'user' => $user
                ], 401); 
            else
                return response()->json([
                    'success' => true,
                    'message' => "This user account has been created successfully",
                    'user' => $user
                ], 200); 
        } else {
            return response()->json([
                'success' => true,
                'message' => "You don't have the permission to this action",
            ], 401); 
        }    
    }

    //make user an admin
    public function MakeAdmin($id, $type){

        $auth_user = Auth::user()->type;

        if($auth_user == 0){
            $user = User::where('id', $id)->update(['type', $type]);

            if(!$user) 
                return response()->json([
                    'success' => true,
                    'message' => "Unable to make this user an admin.",
                    'user' => $user
                ], 401); 
            else
                return response()->json([
                    'success' => true,
                    'message' => "This user account type has been changed successfully.",
                    'user' => $user
                ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "You don't have the permission to this action",
            ], 401); 
        }         
    }

    //ban user by admin

    public function BanUser($id){
        
        $auth_user = Auth::user()->type;
        
        if($auth_user == 0){
            $user = User::select()->where('id', $id)->update(['status' => "inactive"]);

            if(!$user)
                return response()->json([
                    'success' => true,
                    'message' => "Unable to ban this user.",
                    'user' => $user
                ], 401); 
            else 
                return response()->json([
                    'success' => true,
                    'message' => "This user account has been banned successfully.",
                    'user' => $user
                ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "You don't have the permission to this action",
            ], 401); 
        }                  
    }

    public function password_change($id, $token){
        $user = User::where("id", $id)->where("api_token", $token)
        ->update(["password", bcrypt($request->password)]);

        return response()->json([
            "success" => true,
            "messgae" => "Password changes successfully!"
        ],200);
    }

    public function updateUser(Request $request){
        if(Auth::user()){

            if("Bearer ".Auth::user()->api_token != $request->header('Authorization')){
                return response()->json([
                    'success' => false,
                    'message' => "Unauthorized user - you dont have the permission to update this post",
                ], 401);
            }

            $user = User::find(Auth::user()->id);
            if($request->username)
            $user->username = $request->username;
            if($request->name)
            $user->name = $request->name;
            if($request->gender)
            $user->gender = $request->gender;
            if($request->profession)
            $user->profession = $request->profession;
            if($request->location)
            $user->location = $request->location;
            if($request->dob)
            $user->dob = $request->dob;
            if($request->profile_image)
            $user->profile_image = $request->profile_image;
            if($request->phone)
            $user->phone = $request->phone;
            if($request->email)
            $user->email = $request->email;
            if($request->bank_name)
            $user->bank_name = $request->bank_name;
            if($request->bank_account_name)
            $user->bank_account_name = $request->bank_account_name;
            if($request->bank_account_number)
            $user->bank_account_number = $request->bank_account_number;
            $user->save();

            return response()->json([
                "success" => true,
                "data" => $user
            ],200);

        } else {
            return response()->json([
                'success' => false,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
            ], 401); 
        }
    }
    public function getAllUser(){
        if(Auth::user()){
            $user = User::All();

            return response()->json([
                'success' => true,
                'data' => $user,
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
                'user' => $user
            ], 401); 
        }
    }

    public function GetUserDetailsById($id){
        if(Auth::user()){
            
            $user = User::find($id);
            $post = $user->post;
            $view = 0;
            $likes = 0;
            for ($i = 0; $i < sizeof($post); $i++ ){
                $_post = Posts::find($post[$i]->id);
                $view += $_post->views;
                $likes += $_post->likes;
            }
            
            return response()->json([
                'success' => true,            
                'user' => User::find($id),
                'views' => $view,
                'likes' => $likes
            ], 200);
        }
        else{
            return response()->json([
                'success' => false,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
                'user' => $user
            ], 401); 
        }
    }

    public function GetUserDetailsByPhone($phone){
        $user = User::select()->where('phone', $phone)->get();

        return response()->json([
            'success' => true,            
            'user' => $user
        ], 200);
    }

    public function GetUserDetailsByEmail($email){
        $user = User::select()->where('email', $email)->get();

        return response()->json([
            'success' => true,            
            'user' => $user
        ], 200);
    }

    public function DeleteUserById($id){

        $auth_user = Auth::user()->type;

        if($auth_user == 0){
            $user = User::where('id', $id)->delete();

            if(!$user)
                return response()->json([
                    'success' => true,            
                    'message' => "Unable to delete this user"
                ], 401);
            else
                return response()->json([
                    'success' => true,            
                    'message' => "User deleted successfully."
                ], 200);
        }else{
            return response()->json([
                'success' => true,            
                'message' => "You don't the permission to perform this action."
            ], 401);
        }
    }
}

