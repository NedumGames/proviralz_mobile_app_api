<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Transaction;
use App\User;

class TransactionController extends Controller
{
    //
    public function makeWithdrawal(Request $request){
        if(Auth::user()){
            if("Bearer ".Auth::user()->api_token != $request->header('Authorization')){
                return response()->json([
                    'success' => true,
                    'message' => "Unauthorized user - you dont have the permission to update this post",
                ], 401);
            }

            $transaction = new Transaction();
            $transaction->user_id = Auth::user()->id;
            $transaction->amount = $request->amount;
            $transaction->location = $request->location;
            $transaction->bankname = $request->bankname;
            $transaction->bankaccountname = $request->bankaccountname;
            $transaction->bankaccountnumber = $request->bankaccountnumber;
            $transaction->status = "PENDING";
            $transaction->save();

            return response()->json([
                'success' => true,
                'message' => "Withdrawals initialized successfully.",
                'user' => $transaction
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
            ], 401); 
        }
    }

    public function ApproveWithDrawals($id){
        if(Auth::user()){

            $transaction = Transaction::find($id);
            $transaction->status = "APPROVED";
            $transaction->save();

            return response()->json([
                'success' => true,
                'message' => "Withdrawals approved successfully.",
                'user' => $transaction
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
            ], 401); 
        }
    }
    public function viewWithdrawals(){
        if(Auth::user()){

            $transaction = Transaction::all();

            return response()->json([
                'success' => true,
                'data' => $transaction
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
            ], 401); 
        }
    }
    public function viewMyTransaction(Request $request){
        if(Auth::user()){
            
            if("Bearer ".Auth::user()->api_token != $request->header('Authorization')){
                return response()->json([
                    'success' => true,
                    'message' => "Unauthorized user - you dont have the permission to update this post",
                ], 401);
            }
            
            $user_id = Auth::user()->id;
            $transaction = Transaction::where("user_id", $user_id)->get();

            return response()->json([
                'success' => true,
                'data' => $transaction
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
            ], 401); 
        }
    }
    public function viewWithdrawalsById($id){
        if(Auth::user()){

            $transaction = Transaction::find($id);

            return response()->json([
                'success' => true,
                'data' => $transaction
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
            ], 401); 
        }
    }
}
