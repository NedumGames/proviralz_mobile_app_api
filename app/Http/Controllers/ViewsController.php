<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;
use App\User;
use Auth;
use App\Comments;
use App\Likes;
use App\Views;
use App\Activity;

class ViewsController extends Controller
{
    public function CreateViews($post_id, Request $request){
        if(Auth::user()){
            if("Bearer ".Auth::user()->api_token != $request->header('Authorization')){
                return response()->json([
                    'success' => true,
                    'message' => "Unauthorized user - you dont have the permission to update this post",
                ], 401);
            }

            $check_if_viewed = Views::where("user_id", Auth::user()->id)
            ->where("posts_id", $post_id)->get();  

            if(sizeof($check_if_viewed) > 1){
                return response()->json([
                    'success' => true,
                    'message' => "invalid action - this user has already viewed this post.",
                ], 200);
            }

            $views = new Views();
            $views->user_id = Auth::user()->id;
            $views->posts_id = $post_id;
            $views->save();

            $post = Posts::find($post_id);
            $post->views += 1;
            $post->save(); 

            return response()->json([
                'success' => true,
                'data' => $views,
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
            ], 401); 
        }
    }

    public function GetViewsByPost($post_id, Request $request){
        if(Auth::user()){            

            $my_views = Views::where('posts_id', $post_id)->get();
            $post = Posts::find($post_id);
            //check if count if divisible by the amount of views
            $owner = Posts::find($post_id);
            $view_count = count($my_views);

            //get the views and divide the answer by the constant views of 500
            $unit_views = $view_count/5;

            if($unit_views >= 1){                    
                $post->earnings = $unit_views * 1000;
                $post->save();
            }

            return response()->json([
                'success' => true,
                'data' => $my_views,
                'count' => $view_count,
                'post_earnings' => $post->earnings,
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
            ], 401); 
        }
    }
}
