<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;
use App\User;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Comments;
use App\Likes;
use App\Activity;

class LikesController extends Controller
{
    public function CreateLike(Request $request){
        if(Auth::user()){
            if("Bearer ".Auth::user()->api_token != $request->header('Authorization')){
                return response()->json([
                    'success' => true,
                    'message' => "Unauthorized user - you dont have the permission to update this post",
                ], 401);
            }
            $likeExist = Likes::where("posts_id", $request->post_id)->get();
            if(sizeof($likeExist) > 0){
                $likes = Likes::where("posts_id", $request->post_id)->delete();
                
                $post = Posts::find($request->post_id);
                $post->likes -= 1;
                $post->save(); 
                
                $activity = Activity::where("posts_id", $request->post_id)->delete();
                
                return response()->json([
                    'success' => true,
                    'message' => "deleted successfully",
                    "data" => $likeExist
                ], 200);
            } else {
                $likes = new Likes();
                $likes->user_id = Auth::user()->id;
                $likes->posts_id = $request->post_id;
                if($request->comment_id)
                $likes->comment_id = $request->comment_id;
                $likes->save();
    
                $post = Posts::find($request->post_id);
                $post->likes += 1;
                $post->save(); 
    
                
                $activity = new Activity();
                $activity->user_id = Auth::user()->id;
                $activity->owner_id = $post->user_id;
                $activity->activity = "LIKE";
                $activity->posts_id = $request->post_id;
                $activity->save();
    
                return response()->json([
                    'success' => true,
                    'data' => $likes,
                ], 200);
            }
            
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
            ], 401); 
        }
    }
    
    public function CreateDownloads(Request $request){
        if(Auth::user()){
            if("Bearer ".Auth::user()->api_token != $request->header('Authorization')){
                return response()->json([
                    'success' => true,
                    'message' => "Unauthorized user - you dont have the permission to update this post",
                ], 401);
            }

            $downloadExist = DB::table("downloads")
                ->where("post_id", $request->post_id)
                ->where("owner_id", $request->owner_id)->get();
            

            if($downloadExist != "[]" ) {
                return response()->json([
                    'success' => true,
                    'message' => "This song has been added to your download list.",
                ], 200);
            } else {
                
                $data = [
                    'post_id' => $request->post_id,
                    'owner_id' => $request->owner_id
                ];
                
                $download = DB::table("downloads")->insert($data);

                return response()->json([
                    'success' => true,
                    'data' => $download,
                ], 200);    
            }
            
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
            ], 401); 
        }   
    }
    
    public function viewDownloads($owner, Request $request){
        if(Auth::user()){
            if("Bearer ".Auth::user()->api_token != $request->header('Authorization')){
                return response()->json([
                    'success' => true,
                    'message' => "Unauthorized user - you dont have the permission to update this post",
                ], 401);
            }

            $downloadExist = DB::table("downloads")->where("owner_id", $owner)->get();
            $alldownloads = [];
            
            for ($i = 0; $i < sizeof($downloadExist); $i++){
                $post = Posts::find($downloadExist[$i]->post_id);   
                array_push($alldownloads, $post);
            }

            return response()->json([
                'success' => true,
                'downloads' => $alldownloads,
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
            ], 401); 
        }   
    }

    public function DeleteLikes($id, Request $request){
        if(Auth::user()){
            if("Bearer ".Auth::user()->api_token != $request->header('Authorization')){
                return response()->json([
                    'success' => true,
                    'message' => "Unauthorized user - you dont have the permission to update this post",
                ], 401);
            }
            
            $likes =  Likes::find($id);   
            $post = Posts::find($likes->posts_id);
            $post->comments -= 1;
            $post->save();

            $likes->delete();

            return response()->json([
                'success' => true,
                'message' => "Likes deleted successfully",
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
            ], 401); 
        }
    }
    
    public function DeleteDownload($userid, $id, Request $request){
        if(Auth::user()){
            if("Bearer ".Auth::user()->api_token != $request->header('Authorization')){
                return response()->json([
                    'success' => true,
                    'message' => "Unauthorized user - you dont have the permission to update this post",
                ], 401);
            }

            $downloadExist = DB::table("downloads")
                ->where("post_id", $id)->where("owner_id", $userid)->delete();
            

                return response()->json([
                    'success' => true,
                    'message' => "This song has been removed from your download list.",
                    "data" => $id
                ], 200);
            
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
            ], 401); 
        } 
    }
}
