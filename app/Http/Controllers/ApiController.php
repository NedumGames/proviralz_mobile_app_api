<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterAuthRequest;
use App\User;
use Auth;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ApiController extends Controller
{
    public $loginAfterSignUp = true;

    public function sendpin($name, $email, $phone, $pin)
    {
        $user = User::where('email', $email)->where('phone', $phone)->first();
        if(!$user){
         $to = $email;
         $subject = "Proviralz registration pin.";
         
         $message = "<b>Hi ".$name.",</b>";
         $message .= "<p>Use this pin <h2><b>".$pin."</b></h2> to complete your registration process.</p>";
         $message .= "<p>If this mail is not meant for you, kindly ignore, Thank you.</p>";
         
         $header = "From:no-reply@proviralz.com \r\n";
         $header .= "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n";
         
         mail ($to,$subject,$message,$header);
        
        return response()->json([
                'success' => true,
                'pin' => $pin,
                'email' => $email,
            ], 200);   
        } else {
            return response()->json([
                'success' => false,
                'message' => "This user already exist." ,
            ], 200);  
        }
    }

    public function register(Request $request){
        $user = User::where('email', $request->email)->where('phone', $request->phone)->first();

        if(!$user){
            $user = new User();
            $user->name = $request->name;
            $user->username = $request->username;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->gender = $request->gender;
            $user->password = bcrypt($request->password);
            $user->type = $request->type;
            $user->api_token = $token = bin2hex(openssl_random_pseudo_bytes(64));
            $user->save();

            return response()->json([
                'success' => true,
                'token' => $token,
                'user' => $user
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'this user already exist.',
            ], 201);
        }
    }
    
    function random_str(int $length = 64,string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'): string {
        if ($length < 1) {
            throw new \RangeException("Length must be a positive integer");
        }
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces []= $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }
    
    function random_num( int $length = 10, string $keyspace = '0123456789'): string {
        if ($length < 1) {
            throw new \RangeException("Length must be a positive integer");
        }
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces []= $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }
    
    public function googleLogin(Request $request){
        $input = $request->only('email', 'password');
        $token = "";

        if (Auth::attempt($input))
        {   
            $token = bin2hex(openssl_random_pseudo_bytes(64));
            $user_token_save = User::where('email', Auth::user()->email)->update(['api_token' => $token]);         
            $user = User::where('email', Auth::user()->email)->first();

            return response()->json([
                'success' => true,
                'user' => $user,
                'token' => $token,            
            ], 200);
        }else{
            $exist = User::where('email', $request->email)->get();
            
            if(sizeof($exist) < 1){
                return response()->json([
                    'success' => false,
                    'message' => "This login details don't exist. Try registerin.",
                ], 401);
            }
            
            $user = User::where('email', $request->email)->update([
                'password' => bcrypt($request->password),
                'api_token' => $token = bin2hex(openssl_random_pseudo_bytes(64)),
                
                ]);

            return response()->json([
                'success' => true,
                'token' => $token,
                'user' => $exist
            ], 200);
        }
    }
    
    public function googleRegister(Request $request){
        $user = User::where('email', $request->email)->orWhere('phone', $request->phone)->first();
        
        if(!$user){
            $user = new User();
            $user->name = $request->name;
            $user->username = $this->random_str(8);
            $user->phone = "000".$this->random_num(8);
            $user->email = $request->email;
            $user->profile_image = $request->photo;
            $user->gender = "other";
            $user->password = bcrypt($request->password);
            $user->type = $request->type;
            $user->api_token = $token = bin2hex(openssl_random_pseudo_bytes(64));
            $user->save();

            return response()->json([
                'success' => true,
                'token' => $token,
                'user' => $user
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'this user already exist.',
            ], 201);
        }
    }

    public function password_change($id, $token){
        $user = User::where("id", $id)->update(["api_token" => $token]);
    }

    public function resetPassword_OLD($email){

        $user = User::where("email", $email)->get();
        if($user){
            $data = [
                "username" => $user->username,
                "token" => bin2hex(openssl_random_pseudo_bytes(64)),
                "id" => $user->id
            ];
   
            Mail::send('email.password_reset', $data, function($message) {
                $message->to($email, $user->username)->subject('Password Reset link');
                $message->from('no-reply@proviralz.com','Proviralz team');
            });

            return response()->json([
                'success' => true,
                'message' => "Password reset link has been sent to your mail box.",
            ], 200);
        } else{
            return response()->json([
                'success' => false,
                'message' => 'User not found!',
            ], 401);
        } 
        
    }
    
    public function login(Request $request)
    {
        $input = $request->only('email', 'password');
        $token = "";

        if (Auth::attempt(array('email' => $request->email, 'password' => $request->password)))
        {   
            $token = bin2hex(openssl_random_pseudo_bytes(64));
            $user_token_save = User::where('email', Auth::user()->email)->update(['api_token' => $token]);         
            $user = User::where('email', Auth::user()->email)->first();
            
            // ;//$user->createToken('access_token')->accessToken;
            // if($user->status == "inactive"){
            //     return response()->json([
            //         'success' => false,
            //         'message' => "This account has not been verified, check your mail for the verification link.",
            //     ], 200);
            // }

            return response()->json([
                'success' => true,
                'user' => $user,
                'token' => $token,            
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "Your login details don't match.",
            ], 401);
        }           
    }    

    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
        
        $user = User::where('id', Auth::user()->id)->update(['api_token' => ""]);
        Auth::logout();

        if(!$user)
            return response()->json([
                'success' => false,
                'message' => 'Unable to log user out.'
            ], 401);
        else
            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ], 200);

    }
    
    public function ForgetPassword($email, $pin){
        $user = User::where('email', $email)->first();

        if($user){

         $to = $email;
         $subject = "Proviralz Password reset pin.";
         
         $message = "<b>Hi ".$user->name.",</b>";
         $message = "<p>Use this pin <h2><b>".$pin."</b></h2> to complete your password recovery process.</p>";
         $message .= "<p>If this mail is not meant for you, kindly ignore, Thank you.</p>";
         
         $header = "From:no-reply@proviralz.com \r\n";
         $header .= "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n";
         
         User::where('email', $email )->update([
            'remember_token' => $pin
        ]);
         
         mail ($to,$subject,$message,$header);
        
        return response()->json([
                'success' => true,
            ], 200);   
            
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Email not registered!',
            ], 401);
        } 
    }
    
    public function ResetPassword(Request $request){
        $pin = $request->pin;
        $password = $request->password;
        
        $user = User::where('remember_token', $pin )->update([
            'password' => bcrypt($request->password),
            'remember_token' => ""
        ]);
        
        if($user)
            return response()->json([
                'success' => true,
                'message' => "Password reset link has been sent to your mail box.",
            ], 200);
        else
            return response()->json([
                'success' => false,
                'message' => 'User not found!',
            ], 401);
    }
    
}