<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;
use App\User;
use Auth;
use App\Comments;
use App\Likes;
use App\Activity;

class ActivityController extends Controller
{
    public function getActivity($owner_id, Request $request){
        if(Auth::user()){
            if("Bearer ".Auth::user()->api_token != $request->header('Authorization')){
                return response()->json([
                    'success' => true,
                    'message' => "Unauthorized user - you dont have the permission to view this activity.",
                ], 401);
            }

            $activity = Activity::where("owner_id", $owner_id)->get();
            $main_activity = [];
            foreach($activity as $action){
                $actor = User::find($action->user_id);
                $post = Posts::find($action->posts_id);
                array_push($main_activity, [
                    "actor" => $actor->username,
                    "actor_pic" => $actor->profile_image,
                    "activity" => $action->activity,
                    "post" => $post->cover,
                ]
                );
            }

            return response()->json([
                'success' => true,
                'data' => $main_activity
            ], 200);
        }  else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
            ], 401); 
        }
    }
}
