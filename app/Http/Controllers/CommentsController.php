<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;
use App\User;
use Auth;
use App\Comments;
use App\Activity;

class CommentsController extends Controller
{
    public function CreateComments(Request $request){
        if(Auth::user()){
            // if("Bearer ".Auth::user()->api_token != $request->header('Authorization')){
            //     return response()->json([
            //         'success' => true,
            //         'message' => "Unauthorized user - you dont have the permission to update this post",
            //     ], 401);
            // }
            
            $comments = new Comments();
            $comments->user_id = Auth::user()->id;
            $comments->posts_id = $request->post_id;
            if($request->commend_id)
            $comments->comment_id = $request->comment_id;
            $comments->comments = $request->comment;
            $comments->save();

            $post = Posts::find($request->post_id);
            $post->comments += 1;
            $post->save(); 

            $activity = new Activity();
            $activity->user_id = Auth::user()->id;
            $activity->owner_id = $post->user_id;
            $activity->activity = "COMMENT";
            $activity->posts_id = $post->id;
            $activity->save();
            

            return response()->json([
                'success' => true,
                'data' => $comments,
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
            ], 401); 
        }
    }

    public function DeleteComment($id, Request $request){
        if(Auth::user()){
            if("Bearer ".Auth::user()->api_token != $request->header('Authorization')){
                return response()->json([
                    'success' => true,
                    'message' => "Unauthorized user - you dont have the permission to update this post",
                ], 401);
            }
            
            $deletecomment =  Comments::find($id);   
            $post = Posts::find($deletecomment->posts_id);
            $post->comments -= 1;
            $post->save();                        

            $activity = Activity::where("post_id", $post->id)->delete();
            $deletecomment->delete();

            return response()->json([
                'success' => true,
                'message' => "Comments deleted successfully",
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
                'user' => $user
            ], 401); 
        }
    }

    public function getAllComments($post_id){
        if(Auth::user()){
            $post = Posts::find($post_id);

            if(!$post){
                return response()->json([
                    'success' => true,                    
                    'data' => []
                ], 401);
            }
            $comments = $post->comment;

            $finalcomment = [];
            
            
            for( $i = 0; $i < sizeof($comments); $i++){
                $username = User::find($comments[$i]->user_id)->username;
                $profile_image = User::find($comments[$i]->user_id)->profile_image;
                $my_comment = $comments[$i]->comments;
                 $finalcomment[$i] = [
                     "username"=> $username,
                     "profile_image" => $profile_image,
                     "comment" => $my_comment
                     ];
            }

            return response()->json([
                'success' => true,
                'data' => $finalcomment
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => "Authentication error: Make sure you are logged in to perform this action.",
                'user' => $user
            ], 401); 
        }
    }
}
