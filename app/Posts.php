<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comments;
use App\Likes;
use App\Views;

class Posts extends Model
{
    protected $fillable = [
        'description', 'cover', 'file_path', 'genre'
    ];   
    
    /**
     * Get all the associated posts
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comment()
    {
        return $this->hasMany(Comments::class);
    }

    /**
     * Get all the associated likes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes()
    {
        return $this->hasMany(Likes::class);
    }

    /**
     * Get all the associated views
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function views()
    {
        return $this->hasMany(Views::class);
    }
    
    public function user() {
        return $this->belongsTo('User');
    }
}
