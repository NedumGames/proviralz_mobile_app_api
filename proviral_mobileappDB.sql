-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 05, 2020 at 01:57 PM
-- Server version: 5.7.31-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proviral_mobileappDB`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `activity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `posts_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `user_id`, `owner_id`, `activity`, `posts_id`, `created_at`, `updated_at`) VALUES
(1, 15, 17, 'like', 11, NULL, NULL),
(2, 14, 17, 'comment', 11, NULL, NULL),
(3, 13, 17, 'like', 11, NULL, NULL),
(4, 14, 17, 'comment', 11, NULL, NULL),
(5, 13, 17, 'like', 11, NULL, NULL),
(6, 14, 17, 'comment', 11, NULL, NULL),
(18, 18, 15, 'LIKE', 7, '2020-07-05 21:55:08', '2020-07-05 21:55:08'),
(19, 18, 17, 'LIKE', 14, '2020-07-06 06:59:12', '2020-07-06 06:59:12'),
(20, 15, 17, 'COMMENT', 14, '2020-07-06 08:25:03', '2020-07-06 08:25:03'),
(21, 18, 17, 'COMMENT', 14, '2020-07-06 10:33:27', '2020-07-06 10:33:27'),
(22, 18, 17, 'COMMENT', 14, '2020-07-06 10:33:33', '2020-07-06 10:33:33'),
(23, 18, 17, 'COMMENT', 14, '2020-07-06 10:37:22', '2020-07-06 10:37:22'),
(24, 15, 17, 'COMMENT', 14, '2020-07-06 11:15:11', '2020-07-06 11:15:11'),
(25, 18, 17, 'COMMENT', 14, '2020-07-06 14:13:24', '2020-07-06 14:13:24'),
(26, 23, 15, 'LIKE', 10, '2020-07-20 20:19:21', '2020-07-20 20:19:21'),
(27, 25, 25, 'COMMENT', 17, '2020-07-28 19:51:05', '2020-07-28 19:51:05'),
(28, 25, 25, 'COMMENT', 17, '2020-07-28 19:51:06', '2020-07-28 19:51:06');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `posts_id` int(11) NOT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `comments` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `likes` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `posts_id`, `comment_id`, `comments`, `likes`, `created_at`, `updated_at`) VALUES
(1, 15, 3, NULL, 'nice and wonderful song', 0, '2020-05-26 01:07:01', '2020-05-26 01:07:01'),
(2, 17, 3, NULL, 'nice and wonderful song', 0, '2020-05-26 01:07:20', '2020-05-26 01:07:20'),
(4, 15, 2, NULL, 'not so good audio', 0, '2020-05-26 01:08:24', '2020-05-26 01:08:24'),
(5, 15, 14, NULL, 'nice song....mad context', 0, '2020-07-06 08:25:03', '2020-07-06 08:25:03'),
(6, 18, 14, NULL, 'Nice concept awesome music', 0, '2020-07-06 10:33:27', '2020-07-06 10:33:27'),
(7, 18, 14, NULL, 'Nice concept awesome music', 0, '2020-07-06 10:33:33', '2020-07-06 10:33:33'),
(8, 18, 14, NULL, 'Nice concept awesome music', 0, '2020-07-06 10:37:22', '2020-07-06 10:37:22'),
(9, 15, 14, NULL, 'nice song....mad context', 0, '2020-07-06 11:15:11', '2020-07-06 11:15:11'),
(10, 18, 14, NULL, 'Testing this part', 0, '2020-07-06 14:13:24', '2020-07-06 14:13:24'),
(11, 25, 17, NULL, 'Dope', 0, '2020-07-28 19:51:05', '2020-07-28 19:51:05'),
(12, 25, 17, NULL, 'Dope', 0, '2020-07-28 19:51:06', '2020-07-28 19:51:06');

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE `downloads` (
  `id` int(11) NOT NULL,
  `post_id` varchar(11) NOT NULL,
  `owner_id` varchar(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `downloads`
--

INSERT INTO `downloads` (`id`, `post_id`, `owner_id`, `created_at`, `modified_at`) VALUES
(1, '14', '15', '2020-07-06 08:23:07', '2020-07-06 08:23:07'),
(2, '13', '15', '2020-07-06 08:49:31', '2020-07-06 08:49:31'),
(3, '11', '18', '2020-07-06 08:54:50', '2020-07-06 08:54:50'),
(4, '14', '18', '2020-07-06 15:03:40', '2020-07-06 15:03:40'),
(5, '10', '23', '2020-07-20 21:19:12', '2020-07-20 21:19:12'),
(6, '11', '23', '2020-07-20 23:21:40', '2020-07-20 23:21:40');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `posts_id` int(11) NOT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `posts_id`, `comment_id`, `created_at`, `updated_at`) VALUES
(12, 18, 7, 1, '2020-07-05 21:55:08', '2020-07-05 21:55:08'),
(13, 18, 14, 1, '2020-07-06 06:59:12', '2020-07-06 06:59:12'),
(14, 23, 10, 1, '2020-07-20 20:19:21', '2020-07-20 20:19:21');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_05_24_142526_posts', 2),
(4, '2020_05_25_130416_posts', 3),
(5, '2020_05_26_012154_comments_12345', 4),
(6, '2020_05_26_021539_likes_101', 5),
(7, '2020_05_26_023346_vies_123', 6),
(8, '2020_05_26_024306_transaction_table', 7),
(9, '2020_05_26_024403_earnings_table', 7),
(10, '2020_05_26_202121_activity_132', 7),
(11, '2020_05_26_211130_activity_111', 8),
(12, '2020_05_26_212909_activity', 9),
(13, '2020_05_26_215550_transaction', 10),
(14, '2020_05_26_222440_transaction111', 11);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `artist` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `audio` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` text COLLATE utf8mb4_unicode_ci,
  `genre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comments` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `likes` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `earnings` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `artist`, `cover`, `description`, `audio`, `title`, `video`, `genre`, `comments`, `status`, `views`, `likes`, `created_at`, `updated_at`, `earnings`) VALUES
(16, 25, 'Elusdave', 'http://res.cloudinary.com/proviral/image/upload/v1595969060/yggvsqsyp0uxxdimsq7m.jpg', 'Please check out my first song', 'http://res.cloudinary.com/proviral/video/upload/v1595969047/sahqchmflsdfxweysbfk.mp3', 'My first song', NULL, 'Hip-Hop', 0, 1, 4, 0, '2020-07-28 19:44:32', '2020-09-16 19:55:55', 0),
(17, 25, 'Elusdave', 'http://res.cloudinary.com/proviral/image/upload/v1595969402/fylvv26mtoer3df4cnky.jpg', 'Elus', 'http://res.cloudinary.com/proviral/video/upload/v1595969379/fddldzekcwnwelks59pk.mp3', 'Elus', NULL, 'Raggae', 2, 1, 4, 0, '2020-07-28 19:50:19', '2020-09-16 19:56:05', 0),
(18, 21, 'Slimbeauty', 'http://res.cloudinary.com/proviral/image/upload/v1597093712/q9x0mthroke4qord8xrg.jpg', 'Love story between two birds with so much complications', 'http://res.cloudinary.com/proviral/video/upload/v1597093608/zxskmnkbukztwxmjzswq.mp3', 'Ejit', NULL, 'Hip-Hop', 0, 1, 4, 0, '2020-08-10 20:09:22', '2020-10-01 11:00:32', 0),
(19, 17, 'Chinyboi', 'http://res.cloudinary.com/proviral/image/upload/v1597645269/jbtc8ube5rsxvyun5nig.png', 'Testing reggae', 'http://res.cloudinary.com/proviral/video/upload/v1597645244/jnwd4jvlffrohnquicdo.mp3', 'Still testing', NULL, 'Raggae', 0, 0, 2, 0, '2020-08-17 05:22:02', '2020-08-19 11:19:51', 0),
(20, 21, 'Slimbeauty', 'Www, videofiles.com', 'Laycon', 'Www.videofiles cm', 'Awafierce', NULL, 'Hip-Hop', 0, 1, 2, 0, '2020-10-01 10:55:06', '2020-10-01 10:58:52', 0),
(21, 21, 'Slimbeauty', 'Www, videofiles.com', 'Laycon', 'Www.videofiles cm', 'Awafierce', NULL, 'Hip-Hop', 0, 1, 1, 0, '2020-10-01 10:55:06', '2020-10-01 11:01:33', 0),
(22, 21, 'Slimbeauty', 'Www, videofiles.com', 'Laycon', 'Www.videofiles cm', 'Awafierce', NULL, 'Hip-Hop', 0, 1, 1, 0, '2020-10-01 10:55:06', '2020-10-01 10:59:16', 0),
(23, 21, 'Slimbeauty', 'Www, videofiles.com', 'Laycon', 'Www.videofiles cm', 'Awafierce', NULL, 'Hip-Hop', 0, 1, 0, 0, '2020-10-01 10:55:06', '2020-10-01 10:55:06', 0),
(24, 21, 'Slimbeauty', 'Www, videofiles.com', 'Laycon', 'Www.videofiles cm', 'Awafierce', NULL, 'Hip-Hop', 0, 1, 0, 0, '2020-10-01 10:55:06', '2020-10-01 10:55:06', 0),
(25, 21, 'Slimbeauty', 'Www, videofiles.com', 'Laycon', 'Www.videofiles cm', 'Awafierce', NULL, 'Hip-Hop', 0, 1, 2, 0, '2020-10-01 10:55:07', '2020-10-01 10:59:56', 0),
(26, 21, 'Slimbeauty', 'Www, videofiles.com', 'Laycon', 'Www.videofiles cm', 'Awafierce', NULL, 'Hip-Hop', 0, 1, 0, 0, '2020-10-01 10:55:08', '2020-10-01 10:55:08', 0),
(27, 21, 'Slimbeauty', 'Www, videofiles.com', 'Laycon', 'Www.videofiles cm', 'Awafierce', NULL, 'Hip-Hop', 0, 1, 0, 0, '2020-10-01 10:55:08', '2020-10-01 10:55:08', 0),
(28, 21, 'Slimbeauty', 'Www, videofiles.com', 'Laycon', 'Www.videofiles cm', 'Awafierce', NULL, 'Hip-Hop', 0, 1, 1, 0, '2020-10-01 10:55:08', '2020-10-01 11:04:18', 0),
(29, 21, 'Slimbeauty', 'Www, videofiles.com', 'Laycon', 'Www.videofiles cm', 'Awafierce', NULL, 'Hip-Hop', 0, 1, 1, 0, '2020-10-01 10:55:48', '2020-10-01 11:09:52', 0),
(30, 21, 'Slimbeauty', 'Itiswell.com', 'Nskamkssls', 'Itiswell.com', 'Lycon', NULL, 'Hip-Hop', 0, 1, 0, 0, '2020-10-01 11:15:14', '2020-10-01 11:15:14', 0),
(31, 21, 'Slimbeauty', 'Itiswell.com', 'Nskamkssls', 'Itiswell.com', 'Lycon', NULL, 'Hip-Hop', 0, 1, 1, 0, '2020-10-01 11:15:24', '2020-10-01 11:15:36', 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bankname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bankaccountname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bankaccountnumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `user_id`, `amount`, `location`, `bankname`, `bankaccountname`, `bankaccountnumber`, `status`, `created_at`, `updated_at`) VALUES
(1, 17, 188192, 'Bdbdbs', 'Jdhdhd', 'Hdhdhdh', '549454645', 'PENDING', '2020-08-17 15:39:49', '2020-08-17 15:39:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verified` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `profession` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `earnings` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `profile_image` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_name` text COLLATE utf8mb4_unicode_ci,
  `bank_account_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_account_name` text COLLATE utf8mb4_unicode_ci,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `background_image` text COLLATE utf8mb4_unicode_ci,
  `type` int(1) NOT NULL DEFAULT '1',
  `withdraws` int(100) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `gender`, `verified`, `profession`, `location`, `dob`, `status`, `earnings`, `profile_image`, `phone`, `username`, `email`, `bank_name`, `bank_account_number`, `bank_account_name`, `email_verified_at`, `password`, `api_token`, `remember_token`, `created_at`, `updated_at`, `background_image`, `type`, `withdraws`) VALUES
(17, 'Unor chinedum', 'male', '0', NULL, NULL, NULL, 'inactive', '0', 'https://res.cloudinary.com/proviral/image/upload/v1595157051/cag9ezjkvbg7yxb7nywc.jpg', '09057336858', 'Chinyboi', 'Chinyart@gmail.com', NULL, NULL, NULL, NULL, '$2y$10$YkzvnHeY.k7eCejk4RjJ0Ohdo31SdwIAWz3fc2NWNdb7lm.se1V0C', '24fe33b03a8b86fa1b0680920ced528e54d12f1d143d02f4020020ed14a22375518281b11fa0d9ea8826631ccc3f31f5e9ed0ed62927505cfe88ef69873b6e32', '4157382', '2020-06-28 08:51:00', '2020-08-17 07:27:46', NULL, 1, 0),
(19, 'Clementina Oyinkolade', 'female', '0', NULL, NULL, NULL, 'inactive', '0', NULL, '08107819471', 'Kofo Tina', 'Tinaoyinkolade32@gmail.com', NULL, NULL, NULL, NULL, '$2y$10$PTVAKvcuaycJJqK0TgO4quZQqbZgXguDa5657ddIEWQdxblO2TZPa', '1a051d51cb595f0b4f6dd3a82324e3c7fd57b606f4af1e31a0b417accabc05543ca3fb8930526772ed80342718e5c417aedbe68c831ee02c46688f5fbc2412ed', NULL, '2020-07-18 13:45:35', '2020-08-10 20:01:20', NULL, 1, 0),
(20, 'Elufidipe David', 'male', '0', NULL, NULL, NULL, 'inactive', '0', NULL, '08140693924', 'Elusdavid', 'elusdavid@gmail.com', NULL, NULL, NULL, NULL, '$2y$10$miJGF9C8v4Ny9jYk/zs1CuCg1WxReKBvhf2zgHrypUUt6hZDNUXJu', 'b8c2f16d9b272234ca2902cbba7f224670afb6e34058a06956bb3cbafb9f7c54203120b1f982f4179b6607464ac6b4499a408c08b0dffeab12c00600fa3fbfd9', NULL, '2020-07-18 14:12:36', '2020-07-18 14:14:01', NULL, 1, 0),
(21, 'Itunu adebayo', 'female', '0', NULL, NULL, NULL, 'inactive', '0', NULL, '07051453157', 'Slimbeauty', 'Itunucomfortade@gmail.com', NULL, NULL, NULL, NULL, '$2y$10$Yy46TzmXUywyGrylUdmCleBTa5gUzNq3o3bH19lD9AKC7TJlb/AVa', 'ba48af166feeb4c4657431d733005dd0881d4a90eaa337d3fea3777be0eea6abc1eb523a87eac9253928b0acacb95821799325590bb50b8f05a260f34fc718ff', NULL, '2020-07-18 14:44:53', '2020-10-01 10:47:46', NULL, 1, 0),
(23, 'Unor Chinedum C.', 'male', '0', NULL, NULL, NULL, 'inactive', '0', 'https://lh3.googleusercontent.com/a-/AOh14Gibt73GpeRXDuOITV6aAj4F30OmAuXssmsT63HZ=s96-c', '00083774723', 'Tester1', 'chinyarts@gmail.com', NULL, NULL, NULL, NULL, '$2y$10$rKtWDpwKdRXLkkwTBKROOeBC6TjGGtpWFKtST7TK6H.LmYs89VwTC', 'c9b6138c14734b724165d903633bd19e535e84ac802681759d90775cac4e987f3c711ad7f63f85d250e270bfb495b29450e5081cb39cf976bd288582953d746b', NULL, '2020-07-19 15:32:09', '2020-07-20 20:48:45', NULL, 1, 0),
(24, 'Big man', 'male', '0', NULL, NULL, NULL, 'inactive', '0', NULL, '09057336857', 'Biggie', 'Nedumstudios.ng@gmail.com', NULL, NULL, NULL, NULL, '$2y$10$j3g4ciR0Wtp2lcr0vZ.z3eQiCavOJMN21Yl4cDQ92vY45bOcWczdu', 'bdef22a2bbaa8cad37db0494dbba24cce5d98f2a40f5b01ccbd643390c5889db256ddcdf9c7588582e4d341168d28e9d833c9c739cf700e2c3656d6d7f81546d', NULL, '2020-07-25 21:11:55', '2020-09-16 19:54:35', NULL, 1, 0),
(25, 'Elus davis', 'male', '0', NULL, NULL, NULL, 'inactive', '0', NULL, '08140693924', 'Elusdave', 'elusdave@gmail.com', NULL, NULL, NULL, NULL, '$2y$10$LE0GdUlaMm0wQVGMhoafMO3iVKW0u.FGJ2zD5l0UCGMu83hqb.ylO', 'bda5164cd5108a51d34d004e4e6bc0d73b5399cf52ba65324ede3fc0882a0633bb5153765cc2da96affc32557628fe6db210cdc7e64396182d380a415baaafe0', NULL, '2020-07-28 19:40:20', '2020-08-04 12:32:36', NULL, 1, 0),
(26, 'Super Admin', 'male', '0', NULL, NULL, NULL, 'inactive', '0', NULL, '09057336857', 'Biggie', 'superadmin@admin.com', NULL, NULL, NULL, NULL, '$2y$10$j3g4ciR0Wtp2lcr0vZ.z3eQiCavOJMN21Yl4cDQ92vY45bOcWczdu', 'e81998943ad09779c2b8980cc899a5c68037d4c96b404357a74f0bc54da4988b1f0c145fa3af7de7e1949078e05acddebfcc3bc53bd52db4107b9ec422195d85', NULL, '2020-07-25 21:11:55', '2020-08-22 14:10:16', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `views`
--

CREATE TABLE `views` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `posts_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `views`
--

INSERT INTO `views` (`id`, `user_id`, `posts_id`, `created_at`, `updated_at`) VALUES
(1, 14, 3, '2020-05-26 09:49:58', '2020-05-26 09:49:58'),
(2, 14, 1, '2020-05-26 09:56:05', '2020-05-26 09:56:05'),
(3, 15, 4, '2020-05-26 10:40:59', '2020-05-26 10:40:59'),
(4, 15, 3, '2020-05-26 18:02:21', '2020-05-26 18:02:21'),
(5, 16, 3, '2020-05-26 18:03:02', '2020-05-26 18:03:02'),
(6, 14, 3, '2020-05-26 09:49:58', '2020-05-26 09:49:58'),
(7, 14, 1, '2020-05-26 09:56:05', '2020-05-26 09:56:05'),
(8, 15, 4, '2020-05-26 10:40:59', '2020-05-26 10:40:59'),
(9, 15, 3, '2020-05-26 18:02:21', '2020-05-26 18:02:21'),
(10, 16, 3, '2020-05-26 18:03:02', '2020-05-26 18:03:02'),
(11, 23, 15, '2020-07-20 23:10:37', '2020-07-20 23:10:37'),
(12, 23, 15, '2020-07-20 23:10:49', '2020-07-20 23:10:49'),
(13, 25, 17, '2020-07-28 19:50:41', '2020-07-28 19:50:41'),
(14, 24, 16, '2020-07-28 22:35:59', '2020-07-28 22:35:59'),
(15, 17, 16, '2020-07-29 13:01:22', '2020-07-29 13:01:22'),
(16, 17, 14, '2020-07-29 13:04:02', '2020-07-29 13:04:02'),
(17, 17, 17, '2020-07-29 13:04:38', '2020-07-29 13:04:38'),
(18, 17, 17, '2020-07-29 13:06:11', '2020-07-29 13:06:11'),
(19, 17, 16, '2020-07-29 13:06:11', '2020-07-29 13:06:11'),
(20, 17, 12, '2020-07-29 13:08:07', '2020-07-29 13:08:07'),
(21, 17, 14, '2020-07-29 15:59:10', '2020-07-29 15:59:10'),
(22, 24, 14, '2020-07-30 09:51:57', '2020-07-30 09:51:57'),
(23, 21, 18, '2020-08-10 20:13:17', '2020-08-10 20:13:17'),
(24, 17, 18, '2020-08-16 12:37:38', '2020-08-16 12:37:38'),
(25, 17, 18, '2020-08-16 12:45:55', '2020-08-16 12:45:55'),
(26, 17, 19, '2020-08-17 07:32:44', '2020-08-17 07:32:44'),
(27, 17, 19, '2020-08-17 20:57:20', '2020-08-17 20:57:20'),
(28, 24, 16, '2020-09-16 19:55:55', '2020-09-16 19:55:55'),
(29, 24, 17, '2020-09-16 19:56:05', '2020-09-16 19:56:05'),
(30, 21, 20, '2020-10-01 10:58:51', '2020-10-01 10:58:51'),
(31, 21, 20, '2020-10-01 10:58:52', '2020-10-01 10:58:52'),
(32, 21, 22, '2020-10-01 10:59:16', '2020-10-01 10:59:16'),
(33, 21, 25, '2020-10-01 10:59:55', '2020-10-01 10:59:55'),
(34, 21, 25, '2020-10-01 10:59:56', '2020-10-01 10:59:56'),
(35, 21, 18, '2020-10-01 11:00:32', '2020-10-01 11:00:32'),
(36, 21, 21, '2020-10-01 11:01:33', '2020-10-01 11:01:33'),
(37, 21, 28, '2020-10-01 11:04:18', '2020-10-01 11:04:18'),
(38, 21, 29, '2020-10-01 11:09:52', '2020-10-01 11:09:52'),
(39, 21, 31, '2020-10-01 11:15:36', '2020-10-01 11:15:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `downloads`
--
ALTER TABLE `downloads`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `views`
--
ALTER TABLE `views`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `views`
--
ALTER TABLE `views`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
